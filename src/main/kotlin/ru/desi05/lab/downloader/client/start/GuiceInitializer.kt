package ru.desi05.lab.downloader.client.start

import com.google.inject.Guice
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.util.Callback
import ru.desi05.lab.downloader.client.Module
import java.net.URL

/**
 * Created by i-mad_000 on 15.02.2017.
 */
fun initInGuiceContainer(resource: URL): Parent {
    val fxmlLoader = FXMLLoader(resource)
    val injector = Guice.createInjector(Module())
    fxmlLoader.controllerFactory = Callback { injector.getInstance(it) }
    val parent = fxmlLoader.load<Parent>()
    return parent
}