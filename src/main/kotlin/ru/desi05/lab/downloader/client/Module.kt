package ru.desi05.lab.downloader.client

import com.google.inject.AbstractModule
import ru.desi05.lab.downloader.client.service.Kukich
import ru.desi05.lab.downloader.client.start.FxEntryPoint

class Module : AbstractModule() {
    override fun configure() {
        bind(Kukich::class.java)
        bind(FxEntryPoint::class.java)
    }

}