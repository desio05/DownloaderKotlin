package ru.desi05.lab.downloader.client.start

import javafx.fxml.FXML
import javafx.scene.control.Button
import ru.desi05.lab.downloader.client.service.Kukich
import javax.inject.Inject

/**
 * Created by i-mad_000 on 15.02.2017.
 */
class FxController @Inject constructor(val kukich: Kukich) {
    @FXML
    lateinit var testButton: Button

    fun initialize() {
        print("Controller working")
        testButton.setOnAction {
            print(kukich.getHello())
        }
    }
}