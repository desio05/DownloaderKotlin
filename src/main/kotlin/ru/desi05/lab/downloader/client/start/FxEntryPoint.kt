package ru.desi05.lab.downloader.client.start

import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage


class FxEntryPoint : Application() {
    override fun start(primaryStage: Stage?) {
        val mainMarkup = FxEntryPoint::class.java.classLoader.getResource("view/mainWindow.fxml")
        val parent = initInGuiceContainer(mainMarkup)

        val scene = Scene(parent)
        primaryStage?.scene = scene
        primaryStage?.show()
    }

}

fun main(args: Array<String>) {
    Application.launch(FxEntryPoint::class.java, *args)
}
